startr
======

A simple personal homepage where you can manage your daily tasks &amp; notes 100% privately...

All the magic happens inside your browser, so all you will add here it's for your eyes only! 

> I open-sourced the project because this way anyone can see that **NO** information is sent elsewhere than on your local machine (localStorage). So no, no one's spying on you! you're safe!


###Demo

http://cookies.0010media.com/startr/



###Install

Just clone the project, add the startr.html file somewhere on your disk or on a server and you're ready to go. 


###Features

1. **Todo List** _(in development)_
2. **Notepad** _(in development)_
3. **Import / Export** _(todo)_

#### Other ideas

1. Weather
2. Google Calendar Agenda
3. Links (open your favorite links quicker)
4. ... will add more
 

#### Author(s):
- Cristian CIOFU - www.iamchris.info
